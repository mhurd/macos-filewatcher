#### Begin watching a file, executes script.sh when the watched file changes

`~/macos-filewatcher/watch /path/to/watched/file /path/to/script.sh`

#### End watching a file

`~/macos-filewatcher/unwatch /path/to/watched/file`

#### Invoke from any directory

Add this to the end of your `~/.zshrc` file

`export PATH="$PATH:$HOME/macos-filewatcher"`

Then restart terminal or run this to reload `~/.zshrc` for your terminal session

`source ~/.zshrc`

Then, from any directory, you can run `watch` or `unwatch` without specifying their path

`watch /path/to/watched/file /path/to/script.sh`

`unwatch /path/to/watched/file`
